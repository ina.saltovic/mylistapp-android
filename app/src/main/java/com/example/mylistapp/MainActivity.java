package com.example.mylistapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    DatabaseHelper db;

    ListView listOfNames;
    Button addData;

    ArrayList<Person> dataList;
    ArrayAdapter adapter;

    Person personToBeRemoved;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new DatabaseHelper(this);

        dataList = new ArrayList<>();

        addData = findViewById(R.id.addButton);
        listOfNames  = findViewById(R.id.listOfNames);

        addData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAddScreen();
            }
        });

        listOfNames.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                deletePerson(position);
                return true;
            }
        });


        listNames();
    }

    private void listNames() {
        Cursor cursor = db.getDataFromDatabase();

        if(cursor.getCount() == 0) {
            Toast.makeText(this, "No names in database.", Toast.LENGTH_SHORT).show();
        }
        else{
            while (cursor.moveToNext()){
                dataList.add(new Person(cursor.getString(0), cursor.getString(1), cursor.getString(2)));
            }
            adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, dataList);
            listOfNames.setAdapter(adapter);
        }
    }

    private void deletePerson(int position){
        personToBeRemoved = dataList.get(position);
        AlertDialog.Builder msgBox = new AlertDialog.Builder(MainActivity.this);
        msgBox.setTitle("Deleting person");
        msgBox.setMessage("Are you sure you want to delete this person?");
        msgBox.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dataList.remove(personToBeRemoved);
                adapter.notifyDataSetChanged();
                db.deleteDataFromDatabase(personToBeRemoved.getId());
                Toast.makeText(getApplicationContext(), "Person deleted.", Toast.LENGTH_LONG).show();
                Cursor cursor = db.getDataFromDatabase();
                if(cursor.getCount() == 0)
                    Toast.makeText(getApplicationContext(), "No names in database.", Toast.LENGTH_LONG).show();
            }
        });
        msgBox.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //do nothing
            }
        });

        msgBox.setCancelable(true);
        msgBox.create().show();
    }

    public void openAddScreen(){
        Intent intent = new Intent(this, AddActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }
}
