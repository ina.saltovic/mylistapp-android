package com.example.mylistapp;

public class Person {
    private String id;
    private String name;
    private String lastName;

    public Person(String _id, String _name, String _lastName){
        id = _id;
        name = _name;
        lastName =_lastName;
    }

    public String getId(){
        return id;
    }

    @Override
    public String toString(){
        return name + " " + lastName;
    }

}
