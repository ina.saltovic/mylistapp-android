package com.example.mylistapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "Names.db";
    private static final String DB_TABLE = "Name";

    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String LASTNAME = "lastName";

    private static final String CREATE_TABLE = "CREATE TABLE " + DB_TABLE + " (" +
            ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            NAME + " TEXT, " +
            LASTNAME + " TEXT )";

    public DatabaseHelper(Context context){
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DB_TABLE);
        onCreate(db);
    }

    public boolean insertDataInDatabase(String name, String lastName){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(NAME, name);
        contentValues.put(LASTNAME, lastName);

        long result = db.insert(DB_TABLE, null, contentValues);

        return result != -1;
    }

    public Cursor getDataFromDatabase(){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + DB_TABLE;
        Cursor cursor = db.rawQuery(query, null);

        return cursor;
    }

    public void deleteAllFromDatabase(){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "DELETE FROM " + DB_TABLE;
        db.execSQL(query);
    }

    public boolean deleteDataFromDatabase(String id){
        SQLiteDatabase db = this.getReadableDatabase();
        int result = db.delete(DB_TABLE, "id=?", new String[]{id});

        if(result > 0) {
            return true;
        } else{
            return false;
        }
    }

}
