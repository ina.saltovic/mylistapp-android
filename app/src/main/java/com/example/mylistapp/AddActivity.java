package com.example.mylistapp;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class AddActivity extends AppCompatActivity {

    DatabaseHelper db;

    Button saveData;
    EditText nameField, lastNameField;

    ArrayList<String> dataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        db = new DatabaseHelper(this);

        dataList = new ArrayList<>();

        saveData = findViewById(R.id.saveButton);
        nameField = findViewById(R.id.nameField);
        lastNameField = findViewById(R.id.lastNameField);

        saveData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = nameField.getText().toString();
                String lastName = lastNameField.getText().toString();

                if(!name.equals("") && !lastName.equals("") && db.insertDataInDatabase(name, lastName)){
                    Toast.makeText(AddActivity.this, "Person added!", Toast.LENGTH_LONG).show();
                    dataList.clear();
                    openListScreen();
                }
                else{
                    Toast.makeText(AddActivity.this, "Check text fields!", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    public void openListScreen(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
